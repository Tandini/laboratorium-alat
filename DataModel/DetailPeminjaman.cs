﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel
{
    [Table("MstdetailPeminjaman")]
        public class DetailPeminjaman: BaseTable
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long ToolId { get; set; }
        public long BiodataId { get; set; }
        public long DetaiId { get; set; }

        [StringLength(100)]
        public string Keterangan { get; set; }
    }

}
