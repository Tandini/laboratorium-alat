﻿//using DataModel;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using ViewModel;

//namespace DataAccess
//{
//    public class BiodataRepo
//    {
//        public static List<BiodataViewModel> All()
//        {
//            List<BiodataViewModel> result = new List<BiodataViewModel>();
//            using (var db = new Context())
//            {
                
//                result = (from b in db.Biodatas
//                          select new BiodataViewModel
//                          {
//                              Id = b.Id,
//                              Name = b.Name,
//                              Bp = b.Bp,
//                              Deskripsi = b.Deskripsi
//                          }).ToList();
//            }
//            return result;
//        }

//        public static BiodataViewModel ById(int id)
//        {
//            BiodataViewModel result = new BiodataViewModel();
//            using (var db = new Context())
//            {
//                result = (from b in db.Biodatas
//                          where b.Id == id
//                          select new BiodataViewModel
//                          {
//                              Id = b.Id,
//                              Name = b.Name,
//                              Bp = b.Bp,
//                              Deskripsi = b.Deskripsi
//                          }).FirstOrDefault();
//            }
//            return result != null ? result : new BiodataViewModel();
//        }

//        public static ResponseResult UpdateBio(BiodataViewModel entity)
//        {
//            ResponseResult result = new ResponseResult();
//            try
//            {
//                using (var db = new Context())
//                {
//                    #region Create New / Insert
//                    if (entity.Id == 0)
//                    {
//                        Biodata biodata = new Biodata();
//                        biodata.Name = entity.Name;
//                        biodata.Bp = entity.Bp;
//                        biodata.Deskripsi = entity.Deskripsi;
//                        biodata.CreatedBy = entity.Name;
//                        biodata.CreatedDate = DateTime.Now;
//                        db.Biodatas.Add(biodata);
//                        db.SaveChanges();
//                        result.Entity = entity;

//                    }
//                    #endregion Edit
//                    else
//                    {
//                        Biodata biodata = db.Biodatas
//                            .Where(o => o.Id == entity.Id)
//                            .FirstOrDefault();
//                        if (biodata != null)
//                        {

//                            biodata.Name = entity.Name;
//                            biodata.Bp = entity.Bp;
//                            biodata.Deskripsi = entity.Deskripsi;
//                            biodata.CreatedBy = entity.Name;
//                            biodata.CreatedDate = DateTime.Now;

//                            db.SaveChanges();
//                            result.Entity = entity;

//                        }
//                        else
//                        {
//                            result.Success = false;
//                            result.Message = "Category not found";
//                        }
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                result.Success = false;
//                result.Message = ex.Message;
//            }
//            return result;

//        }

//        public static ResponseResult Delete(BiodataViewModel entity)
//        {
//            ResponseResult result = new ResponseResult();
//            try
//            {
//                using (var db = new Context())
//                {
//                    Biodata biodata = db.Biodatas
//                        .Where(o => o.Id == entity.Id)
//                        .FirstOrDefault();
//                    if (biodata != null)
//                    {
//                        db.Biodatas.Remove(biodata);
//                        db.SaveChanges();
//                        result.Entity = entity;
//                    }
//                    else
//                    {
//                        result.Success = false;
//                        result.Message = "Category not found";
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                result.Success = false;
//                result.Message = ex.Message;
//            }
//            return result;
//        }
//    }
//}
