﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class RincianRepo
    {
        public static List<BiodataViewModel> AllBio()
        {
            List<BiodataViewModel> result = new List<BiodataViewModel>();
            using (var db = new Context())
            {

                result = (from b in db.Biodatas
                          select new BiodataViewModel
                          {
                              Id = b.Id,
                              Name = b.Name,
                              Bp = b.Bp,
                              Deskripsi = b.Deskripsi
                          }).ToList();
            }
            return result;
        }

        public static ResponseResult UpdateBio(BiodataViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New / Insert
                    if (entity.Id == 0)
                    {
                        Biodata biodata = new Biodata();
                        biodata.Name = entity.Name;
                        biodata.Bp = entity.Bp;
                        biodata.Deskripsi = entity.Deskripsi;
                        biodata.CreatedBy = entity.Name;
                        biodata.CreatedDate = DateTime.Now;
                        db.Biodatas.Add(biodata);
                        db.SaveChanges();
                        result.Entity = entity;

                    }
                    #endregion Edit
                    else
                    {
                        Biodata biodata = db.Biodatas
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                        if (biodata != null)
                        {

                            biodata.Name = entity.Name;
                            biodata.Bp = entity.Bp;
                            biodata.Deskripsi = entity.Deskripsi;
                            biodata.CreatedBy = entity.Name;
                            biodata.CreatedDate = DateTime.Now;

                            db.SaveChanges();
                            result.Entity = entity;

                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Category not found";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;

        }

        public static List<LaboratoryViewModel> All()
        {
            List<LaboratoryViewModel> result = new List<LaboratoryViewModel>();
            using (var db = new Context())
            {
                result = (from l in db.Laboratories
                          select new LaboratoryViewModel
                          {
                              Id = l.Id,
                              Initial = l.Initial,
                              Name = l.Name,
                              Deskripsi = l.Deskripsi
                          }).ToList();

            }
            return result;
        }

        public static List<ToolViewModel> ByLab(int id)
        {
            List<ToolViewModel> result = new List<ToolViewModel>();
            using (var db = new Context())
            {
                result = (from t in db.Tools
                          join l in db.Laboratories
                          on t.LaboratoryId equals l.Id
                          where t.isDelete == false
                          && t.LaboratoryId == id
                          select new ToolViewModel
                          {
                              Id = t.Id,
                              Name = t.Name,
                              Jumlah = t.Jumlah
                          }).ToList();
            }
            return result;
        }

        public static List<DetailPeminjamanViewModel> ByPeminjam(int Id)
        {
            List<DetailPeminjamanViewModel> result = new List<DetailPeminjamanViewModel>();
            using (var db = new Context())
            {
                result = (from dep in db.DetailPeminjaman
                          join b in db.Biodatas
                          on dep.BiodataId equals b.Id
                          join t in db.Tools
                          on dep.ToolId equals t.Id
                          where dep.isDelete == false
                          && dep.BiodataId == Id
                          select new DetailPeminjamanViewModel
                          {
                              Id = dep.Id,
                              Name = t.Name
                          }).ToList();
            }
            return result;
        }

        public static List<ToolViewModel> ByLaboratory(long LaboratoryId)
        {
            List<ToolViewModel> result = new List<ToolViewModel>();
            using (var db = new Context())
            {
                result = (from t in db.Tools
                          join l in db.Laboratories
                          on t.LaboratoryId equals l.Id
                          where t.LaboratoryId == LaboratoryId
                          select new ToolViewModel
                          {
                              LaboratoryId = l.Id,
                              Id = t.Id,
                              Name = t.Name,
                              Jumlah = t.Jumlah
                          }).ToList();
            }
            return result;
        }

        public static DetailPeminjamanViewModel ByTool(int id)
        {
            DetailPeminjamanViewModel result = new DetailPeminjamanViewModel();
            using (var db = new Context())
            {
                result = (from d in db.Tools
                          join l in db.Laboratories
                          on d.LaboratoryId equals l.Id
                          where d.Id == id
                          select new DetailPeminjamanViewModel
                          {
                              Id = d.Id,
                              Name = d.Name,
                              NameLab = l.Name
                          }).FirstOrDefault();
            }
            return result != null ? result : new DetailPeminjamanViewModel();
        }

        public static ResponseResult Update(DetailViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new Context())
                {
                    #region Create New / Insert
                    if (entity.Id == 0)
                    {
                        Detail detail = new Detail();
                        detail.ToolId = entity.ToolId;
                        detail.BiodataId = entity.BiodataId;
                        detail.CreatedBy = "Dini";
                        detail.CreatedDate = DateTime.Now;
                        db.Details.Add(detail);
                        db.SaveChanges();
                        result.Entity = entity;

                    }
                    #endregion Edit
                    else
                    {
                        Detail detail = db.Details
                            .Where(o => o.Id == entity.Id)
                            .FirstOrDefault();
                        if (detail != null)
                        {
                            detail.ToolId = entity.ToolId;
                            detail.BiodataId = entity.BiodataId;
                            detail.CreatedBy = "Dini";
                            detail.CreatedDate = DateTime.Now;

                            db.SaveChanges();
                            result.Entity = entity;

                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Category not found";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;

        }

        public static ResultOrder Post(DetailViewModel entity)
        {
            ResultOrder result = new ResultOrder();
            try
            {
                using (var db = new Context())
                {
                    Detail oh = new Detail();
                    oh.awal = entity.awal;
                    oh.akhir = entity.akhir;
                    oh.BiodataId = entity.BiodataId;
                    oh.CreatedBy = "Dini";
                    oh.CreatedDate = DateTime.Now;
                    db.Details.Add(oh);
                    db.SaveChanges();
                    foreach (var item in entity.detail)
                    {
                            DetailPeminjaman od = new DetailPeminjaman();
                            od.DetaiId = oh.Id;
                            od.ToolId = item.ToolId;
                            od.BiodataId = oh.BiodataId;
                            od.Keterangan = "Belum Diproses";
                            od.CreatedBy = "Dini";
                            od.CreatedDate = DateTime.Now;
                            db.DetailPeminjaman.Add(od);
                            db.SaveChanges();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }

        public static List<DetailPeminjamanViewModel> AllCek()
        {
            List<DetailPeminjamanViewModel> result = new List<DetailPeminjamanViewModel>();
            using (var db = new Context())
            {
                result = (from dep in db.DetailPeminjaman
                          join b in db.Biodatas
                          on dep.BiodataId equals b.Id
                          join t in db.Tools
                          on dep.ToolId equals t.Id
                          join l in db.Laboratories
                          on t.LaboratoryId equals l.Id
                          where dep.isDelete == false
                          select new DetailPeminjamanViewModel
                          {
                              Id = dep.Id,
                              Bio= b.Name,
                              NoBp = b.Bp,
                              Name = t.Name,
                              NameLab = l.Name,
                              Keterangan = dep.Keterangan
                          }).ToList();
            }
            return result;
        }
    }
}
