﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace WebApplication1.Controllers
{
    public class LaboratoryController : Controller
    {
        // GET: Laboratory
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            return PartialView("_List", RincianRepo.All());
        }
        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(RincianRepo.AllBio(), "Id", "Name");
            return PartialView("_Create", new BiodataViewModel());
        }

        [HttpPost]
        public ActionResult Create(BiodataViewModel model)
        {

            ResponseResult result = RincianRepo.UpdateBio(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult Detail(int Id)
        {
            List<ToolViewModel> model = RincianRepo.ByLab(Id);
            Session["Tipe"] = Id;
            return PartialView("_Detail", model);
        }

        public ActionResult DetailAlat(int Id)
        {
            List<DetailPeminjamanViewModel> model = RincianRepo.ByPeminjam(Id);
            Session["Alat"] = Id;
            return PartialView("_DetailAlat", model);
        }

        public ActionResult Data()
        {
            return PartialView("_Data", RincianRepo.AllBio());
        }

        public ActionResult CreateAlat()
        {
            ViewBag.LaboratoryList = new SelectList(RincianRepo.All(), "Id", "Name");
            ViewBag.ToolList = new SelectList(RincianRepo.ByLaboratory(0), "Id", "Name");
            return PartialView("_CreateAlat", new DetailViewModel());

        }

        public ActionResult GetByLaboratory(long laboratoryId)
        {
            return View(RincianRepo.ByLaboratory(laboratoryId));
        }

        [HttpPost]
        public ActionResult CreateAlat(DetailViewModel model)
        {

            ResponseResult result = RincianRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult SelectAlat(int id)
        {
            DetailPeminjamanViewModel type = RincianRepo.ByTool(id);
            return PartialView("_SelectAlat", type);
        }

        public ActionResult Simpan(DetailViewModel model)
        {
            ResultOrder result = RincianRepo.Post(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    reference = result.Reference
                }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Cek()
        {
            return PartialView("_Cek", RincianRepo.AllCek());
        }

        public ActionResult Info()
        {
            return PartialView();
        }

    }
}