﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class DetailPeminjamanViewModel
    {
        public long Id { get; set; }
        public long ToolId { get; set; }
        public long BiodataId { get; set; }
        public long DetaiId { get; set; }

        [StringLength(100)]
        public string Keterangan { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
        public string NameLab { get; set; }
        public string Bio { get; set; }

        [StringLength(10)]
        public string NoBp { get; set; }


    }
}
